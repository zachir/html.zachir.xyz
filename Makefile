MAIN_DIR_HTML := build/index.html build/blog.html build/about.html
STYLE_CSS := style/main.css
IMAGES_DIR := images/google_logos.jpg images/mozilla_vpn.png
POSTS_DIR_MARKDOWN := $(wildcard posts/*.md)
POSTS_DIR_HTML := build/posts
BUILD_DIR := build/
all: ${BUILD_DIR} ${MAIN_DIR_HTML} build/${STYLE_CSS} build/${IMAGES_DIR} posts ${POSTS_DIR_MARKDOWN}

${BUILD_DIR}:
	mkdir -p ${BUILD_DIR}

build/index.html: index.md templates/template.html
	pandoc index.md --template=templates/template.html -o build/index.html

build/blog.html: blog.2.md templates/template.html
	pandoc blog.2.md --template=templates/template.html -o build/blog.html

blog.2.md: blog.md ${POSTS_DIR_MARKDOWN}
	perl blog_gen.pl

build/about.html: about.md templates/template.html
	pandoc about.md --template=templates/template.html -o build/about.html

build/style/main.css: style/main.css
	mkdir -p build/style
	install style/main.css build/style/main.css

build/images/google_logos.jpg: images/google_logos.jpg
	mkdir -p build/images
	install images/google_logos.jpg build/images/

build/images/mozilla_vpn.png: images/mozilla_vpn.png
	mkdir -p build/images
	install images/mozilla_vpn.png build/images/

.PHONY: posts ${POSTS_DIR_MARKDOWN}

posts: ${POSTS_DIR_MARKDOWN} templates/template.html
	make -C $@

clean:
	rm -rf build/ blog.2.md posts/Makefile

install: all
	cp -rfv build/* /srv/http/
