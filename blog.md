---
title: ZachIR's Blog
author: ZachIR
date:
output:
  html_document:
    template: template.html --- This page contains a link to the blog posts I've made, grouped by subject.
---

This is not to imply that each subject will have even close to the same
number of posts; I just write about whatever I feel like writing about.
However, these 4 categories are the ones that everything should fit into.

