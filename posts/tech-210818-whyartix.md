---
title: ZachIR's Blog - Why Artix?
author: ZachIR
draft: false
date: August 18, 2021
output:
  html_document:
    template: template.html
---
### Why Artix?

I may have mentioned this before, but I am an Artix Linux user by
default (except when I am an OpenBSD user). I have tried many other
Linux distributions (Fedora, Linux Mint, Ubuntu, Arch, Antergos (RIP),
Endeavour, Manjaro, MX, OpenSUSE, Debian, Devuan, a couple of LFS based
distros, NuTyX and Venom). I think it\'s fair to call me a Linux
power-user. No, I haven\'t ever gone down the CentOS (RIP), or Mandriva
(RIP) based distros, but I am pretty comfortable with Linux systems. And
yet, no matter how far I may roam, no matter what I try, I always come
back to Artix. What can I say? The repos are excellent, I can go the
minimal install route and not have to remove a bunch of crap, and leave
unnecessary libraries littering my SSD, I have the entire AUR at my
disposal (even though I typically have to write init scripts for the
daemons I get from the AUR, like auto-cpufreq), I have the one ok part
of systemd (logind) without the rest (I can choose either RUnit, OpenRC,
or S6 as init systems).

But, if I ever need something a little more normie to install, Artix
also comes with a few full Desktop Environment Calamares installatino
options. And, being Arch based, it\'s easy to support Full Disk
Encryption (including an encrypted /boot, which I would like to make a
tutorial for soon, once I get it ironed out).

Additionally, the Artix repositories, while missing certain packages
from the Arch repos, have a LOT of options, enough that the Artix team
was confident enough to stop including the Arch extra and community
repos, by default. Although, the repos are still installable with the
artix-archlinux-support package, and then editing the pacman.conf.

Finally, pretty much every application packaged for Linux is available
in the AUR, which means that when I need to install something, it\'s
generally a snap to get done (no pun here, snappy has a hard systend
dependency, and Artix is a sytemd-free zone).

That unintentional non-existent pun reminds me, Artix also has Flatpak
support, which is nice with some software. For example, installing a
sandboxed version of Steam (which, if I remember correctly, Steam is one
of the small group of apps which is actually sandboxed by any practical
definition, which is a rant I intend to go on another day). It\'s nice
to containerize proprietary spyware, it makes me feel a little better
about using it.

Additionally, the inclusion of such a low-level choice as the init
system is something I enjoy; I love Runit, but I am actually trying
OpenRC right now, and it certainly has its merits. S6 is also an option,
which I\'m going to have to try out at some point as well.
:::

+-----------------------------------+----------------------------------:+
