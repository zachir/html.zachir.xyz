---
title: ZachIR's Blog - Modern Design Sucks, II
author: ZachIR
draft: false
date: August 4, 2021
output:
  html_document:
    template: template.html
---
### \"Modern Design\" is the Worst Thing to Happen to Websites

The Brave web browser advertises itself as loading web pages \"three
times faster than Chrome,\" a claim due very little (if at all) to
rendering web pages faster, but rather due largely to blocking large
chunks of javascript and ads from loading. The CNN homepage (which is
usually the one referenced by Bryan Lunduke) takes a whopping 9.63 MB
and almost a minute to download (at least on my crappy American
internet). As a point of comparison, my homepage takes up 3.34 KB
(according to Librewolf), and Luke Smith\'s [landchad.net
homepage](https://landchad.net) takes up 88.4 KB.

Let\'s look at some other popular websites.

The Youtube homepage takes up 3.03 MB.

The Twitter homepage takes up 4.90 MB.

The Google homepage takes up 1.69 MB.

Invidious actually fares slightly better, at 978.44 KB with Javascript,
and 710.49 KB without (I used the [yewtu.be](https://yewtu.be) instance)

Additionally, Nitter does a lot better than Twitter, at 44.43 KB.

So, why is this? To put it simply, the misuse of javascript.

As much as I dislike it, Javascript is not inherently evil. However, web
developers have become either increasingly lazy, or malicious. This size
issue is either the result of web developers not caring to write good
websites, or an intentional act on the part of web developers to push
people to pay more for faster internet. In either case, it is not good.

Javascript does not, inandof itself, take up very much space; as with
any scripting language, each script takes up one byte per character of
source code. However, web developers typically include libraries, which
take up additional space, or worse, javascript frameworks. And
additionally, there are what I refer to as \"web kiddies\", who take
pre-existing projects (like Wordpress) and bolt more on to them, or add
a bunch of extensions.

Web developers have found it to be much easier to simply grab someone
else\'s code and add it into their website, than to try to write just
what they need, and the result is *Mega*bytes of space being used,
without benefit. In fact, this makes the pages much worse, because of
how much longer they take to download, not only because of the
additional space of javascript libraries, but also having to process
requests to other domains to download more.

Additionally, all of this additional javascript makes the web much more
error-prone. Have you heard of the parable of the dynamically allocated
refridgerator? Basically, it\'s a refridgerator, but everytime you reach
for something inside of it, it moves, and you end up grabbing and
crushing an egg, making a mess, and getting you no closer to what you
wanted to get.

This is what it\'s like using many \"modern\" webpages. I will never
forget the glitch I got when using [Ting\'s website](https://ting.com)
where the website continued to perpetually load more and more \"Sign in
with Amazon\" buttons. Which is annoying enough, but every other time
one would load, it would push everything below them in the website down,
meaining the regular sign-in area was constantly getting moved. And this
is not specific to Ting, this is just the easiest, most concrete example
I have from my own experience.

So, how could this be fixed? Simple: *STATIC WEB PAGES*. You don\'t need
megabytes of javascript to make a login page. As Invidious shows, you
don\'t need any javascript to have a video player and functional YouTube
frontend. And, again, as Nitter shows, you don\'t need javascript to log
in to and interact with a social media platform. And you couldn\'t
convince me that you need megabytes of javascript to load a news site.

But, not only are these websites a massive headache to download, not
only are they a pain to use, not only are they more error-prone, but
they are also a much bigger headache to *maintain*. Web standards are
constantly changing; and javascript is certainly no different. Having
more code, which is programming against an ever-changing standard, means
more code that has to be updated, more frequently. *And* more bugs when
these updates inevitably get overlooked. Which is one of the benefits of
offloading code maintenance to library and framework developers, that
the web kiddie doesn\'t have to manage all of the source code; however,
it would be easier for *everyone* to simply *not need all that source
code in the first place*.

So, in conclusion, the modern web is bloated, and this is a problem for
everyone, not just users. There is more to the \"Modern Design Sucks\"
arguments I have, however, I would like to write about something
different for my next post. Something a little more, positive.
