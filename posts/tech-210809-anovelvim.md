---
title: ZachIR's Blog - Terrestrial NeoVim
author: ZachIR
draft: false
date: August 9, 2021
output:
  html_document:
    template: template.html
---
### Terrestrial NeoVim

Well, I scrapped spacevim. It just didn\'t work out from me; even with
the lazy loading, it was still pretty slow to start on my old laptop
(although, to its credit, it was still speeding up a little a few days
later). What surprises me, though, is that the 436-line, 27-plugin
configuration of neovim is still significantly faster. Which,
incidentally, is what I have been up to today.

I have spent the past few hours (off-and-on) configuring neovim;
exploring all of the options of the plugins I already had installed, as
well as adding a few more. I am writing this article (once again) in
neovim, and I have to compliment a few of the plugins: Goyo especially,
which produces a very nice, distraction-free writing environment. I am
going to have to make a few tweaks to it, such as turning my
auto-suggestion plugins off when Goyo is active, as well as possibly
seeing if I can tweak limelight a bit better, but overall, it is very
/comfy/.

Actually, I am happier with neovim now than I ever have been. It took me
*maybe* around a minute to fix the issue I had with autocompletions in
Goyo, and that is including the time it took to look to see if anyone
else had already done it (which is where I got the fix, of course). And
I have a workaround for limelight (which is a purely cosmetic plugin
anyways), which is just to have an extra empty line between paragraphs.
It is now exactly as I want it. And with only two config files (the main
init.vim, and the separate completion.lua).

I think I shall make a video going over my neovim config, presuming I
ever get around to making any of the video ideas I have. Also, having a
transparent background and a *beautiful* wallpaper tend to make for a
very happy time writing. What would be even better, would be if I could
successfully figure out how to make background windows in dwm disappear
when one is fullscreen/monocle, so I could just have a fullscreen neovim
with a transluscent view of my background (which was provided by
[DT](https://github.com/dwt1/wallpapers), btw).

I could also make a series of articles about it, which is another thing
I probably won\'t end up getting around to. It wouldn\'t be that
difficult, as I tend to document everything as I go, so there is a fair
chance of it happening. It is very useful that neovim lets you source
from lua files in the config, and read configuration options from it, as
lua is a fairly nice configuration language. Of all of the languages I
have some degree of familiarity with, lua would probably be on the upper
end of the middle of the pack.

Thank you for bearing with me, as this article is pretty useless, but I
wanted to write, and this is what came to mind.
