---
title: ZachIR's Blog - Vim In SPACE
author: ZachIR
draft: false
date: August 7, 2021
output:
  html_document:
    template: template.html
---
### Vim In SPACE! - Trying a Vim Distribution

I have been a \*vim-exclusive user since the late 2010\'s, which
(un-)coincidentally was around the time I switched full-time to Linux.
Specifically, neovim is my weapon of choice (based purely on a cursory
examination of the mission statement and repo).

After finding out about Free software from a Richard Stallman talk (how
exactly I got to said talk was a journey inandof itself), which led me
to learning about GNU, and specifically GNU Emacs. At that point, I
considered myself something of a software minimalist, so I decided Emacs
was too bloated for my to try. Then I heard about it again a couple
years later, from Distrotube, and I thought about giving it a try. It
seemed to run just as quickly as vim on his videos.

I specifically heard about a distribution of Emacs which he used, called
Doom Emacs, which was vim-like, and this piqued my interest, so I
installed Emacs on my Antergos install, git cloned the Doom Emacs repo
into \~/.emacs.d (as one does), and finalized the install, before
finally opening it up and\--

it took *forever* to load. Which is fine, I know how it works, the first
open time is the longest, so I messed around a little with the config,
and then closed it without much thought. Then I opened it again to open
some other config file (I forget what), and it still took a really long
time, way longer than my beloved nvim. This is pretty much a nail in the
coffin for most programs for me, unless it has some really strong
benefits.

However, I do like the framework of Doom Emacs, three config files, each
of which has a distinct purpose, which are all the user needs to
interact with in order to configure everything. Even more, Doom Emacs
has great documentation within the distribution itself, so you don\'t
typically have to go to the internet to look up what the keybindings
are.

This has led me to another thought: there is a conceptually similar
distribution of vim, called SpaceVim (inspired by another distro,
SpacEmacs), which should give me the best of both worlds: the great
configuration framework, plus I can continue using my beloved nvim,
*plus* I can have the same configuration in gvim, which is the editor
Qutebrowser uses to edit the source code by default (at least for the
jmatrix plugin).

This leads me to today\'s post: I am going to be taking a SpaceVim
challenge, where I will be using the SpaceVim distribution as my one and
only text editor for some period of time, until either I am totally
comfortable with it, or I can no longer stand it.

Actually, I am writing this article using nvim using SpaceVim, and I
have already found a couple pinch points, although I\'m sure they will
be easy to remove. One is that the autocomplete uses the Enter key by
default, which is annoying when writing HTML, as pressing Enter once
after the \<p> tag (or any other tag) closes the autocomplete instead of
adding another line, so I have to press Enter twice to do so.
