---
title: ZachIR's Blog - Initial Thoughts - Gentoo
author: ZachIR
draft: false
date: August 14, 2021
output:
  html_document:
    template: template.html
---
### Some Initial Thoughts While Installing Gentoo

For those of you who don\'t know, I have been using either Artix Linux,
OpenBSD, or a combination of the two, for most (if not nearly all) of
the past year. Why? Because I love them, and they work. Well. But
sometimes, you get bored with the status quo.

Sometimes, you want to try things the hard way. And if you\'re me,
sometimes you take small benefits in smaller size and resource usage as
an excuse to compile your operating system from source.

Gentoo will not be the first source-based operating system I will have
used, however. I used a (much smaller) LFS-based Linux distro called
Venom Linux, which gives you a minimal base installation through
binaries, and you take it from there with source code. Now, for this
particular distro (which does use BSD-style init scripts, which I like),
there was a bug which meant that I could not get X11 to compile, no
matter what I tried; I found out later that there was a base system +
Xorg install option, so I went with that, and then compiled everything
to go on top of it from source. Not the best experience; especially web
browsers (at a time when I was using firefox, no less) take forever to
compile.

However, what has taken the longest amount of time to compile so far in
this gentoo install is specifically the dependencies for wpa_supplicant.
At the time of writing this, it is 1:22 AM, and I have been compiling
dependencies for wpa_supplicant since 11:12 PM, and I still haven\'t
gotten to the actual wpa_supplicant source code. I had never realized
*how much source code* is required for a simple wifi connection. My
kernel took probably half the amount of time that this has to be
compiled (granted, it was a custom kernel based off of the venerable
\"make localmodconfig\" command).

I decided to go for compiling gentoo after watching both DistroTube and
Brodie Robertson upload videos doing a base install (which Brodie
Robertson [installed live](https://yewtu.be/watch?v=KzWFN_oQ9bc), and DT
[uploaded a prerecorded video](https://yewtu.be/watch?v=2sOfX_Lb1As)).
However, I have wanted to install it long before now; back when I first
started using Arch Linux (specifically Antergos), I heard rumors about a
legendary operating system used by the great hacker 4-chan, called
Gentoo, where the user compiled all of the source code for the OS. I was
intrigued; by this point, I had learned about Free (Libre) software from
a couple of Richard Stallman talks, and ever since then I have enjoyed
the idea of compiling my own software. To this day, there are still
programs which are available as binary packages in the AUR, that I still
prefer to compile myself, and I try to compile every web browser I use
from source if I can (with the exception of Librewolf, the superior fork
of Firefox that still takes forever to compile).

While yes, this does give a good indication of where the bloat is in
software, I also just really enjoy the idea of it. I control every
aspect of updating: I grab the upstream code when I want to, I compile
it when and how I want to, and I install it when and how I want to. I
can change the compile flags to build in or remove features, granted
that is only with options that are provided upstream. But I can also
modify the source code before compiling and installing it, which I
occasionally do if I need to change something (although it\'s more like
all the time with suckless software).

Regardless, I think I\'m going to enjoy Gentoo when I finally get it
installed. I am certainly looking forward to the experience, and the
extra foot that will grow on my neckbeard. After this, I think I will
finally be prepared for the final obstacle in my path: Linux From
Scratch. Of course, first I have to get Gentoo fully installed and
booting.
