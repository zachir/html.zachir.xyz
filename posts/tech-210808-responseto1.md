---
title: ZachIR's Blog - Response to Another Take
author: ZachIR
draft: false
date: August 8, 2021
output:
  html_document:
    template: template.html
---
### I Respond To Someone Else\'s Take

Well, there\'s a first time for everything, I suppose. But I read this
article while doing research, and you know what? I felt like responding
to it. So here it is.

This page was written by an \"average developer,\" which is more than I
can claim to be on the web development front. However, as someone who
was tried multiple times to learn web development, and further as
someone who has given up web development countless times because of
javascript, I feel I can confirm many points the author makes.

Actually, I affirm most of the points they make; as many programming
languages as there are ***for web dev***, there aren\'t any good ones
that aren\'t hacky getting to work. For as many of those languages are
higher level, so much time is spent on writing things that are *not* the
main functionality. Additionally, mostly everyone (except Google\'s pet
Firefox users) is using the exact same web engine (Blink is the web
engine behind Chrome, Edge, Opera, Brave, and many others, and it makes
up between 80-90% of web traffic) uses the same browser engine, *and
yet* it\'s really difficult to make a single web app look good on all
platforms.

There is one specific point they make, though, that I vehemently
disagree with, that all browsers should auto-update by default. Now, I
understand that Zach levels of paranoia are not exactly common among
normal people, but there are some strong reasons to refuse this point.

Firstly, for those who don\'t know, an auto-update in any program is
what security experts like to call a \"backdoor,\" and these are
notoriously hard to protect, especially as the one the user needs the
most protection from is generally the providers. It is pretty
commonplace for a corporation, say Apple, to decide that they need to
make a major change to user\'s devices, let\'s say like downloading an
AI (because those are world-reknown for their accuracy) to look through
*EVERY FILE ON THE USER\'S DEVICE* to look for something bad, let\'s say
CSAM, without notifying the user. Of course, something like that would
be a rediculous invasion of privacy that no one would ever do, right?

Let\'s give another hypothetical. Let\'s say another company, ASUS
perhaps, implements an auto-update system in their computers, and they
don\'t have the best security, so some hackers compromise the update
server, and then, suddenly, everyone with an ASUS device has malware on
their computer that is entirely not their fault (unless you say it\'s
their fault they bought an ASUS device, in which case I would ask if you
could take a look at my sister\'s iPhone).

Auto-updates are a neat trick if your users don\'t know any better
(which, incidentally, is the only time it could be necessary, as these
users probably don\'t update anything either), but for competent users,
we typically don\'t update because we don\'t want to. I put off updating
Librewolf for a couple weeks because I didn\'t want to deal with
Firefox\'s new Proton UI (how about you make a faster browser engine
instead, Mozilla?), and because I know how to mitigate security risks
from my browsing I was ok.

If I see any form of auto-update in a program, I immediately reconsider
using it. I have put a lot of work into making sure *I* control my
computer and its software, and I won\'t give that up to just anyone (but
*especially* not Mozilla, Google, or Microsoft.

Now, if you just read my response, you\'ll probably think that this was
some major point in the article; and you\'d be mistaken. It is a minor
point in a list of suggestions to improve web development, basically all
of the others I agreed with. But, this is the part I had the most to say
about. So, there you go.
