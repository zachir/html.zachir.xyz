---
title: ZachIR's Blog - Writing Blog Posts In Markdown
author: ZachIR
draft: false
date: August 24, 2021
output:
  html_document:
    template: template.html
---
### Writing Blog Posts In Markdown

I have found a new method for writing my blog posts, which I will be giving a
shot today (and hopefully keep, as I just migrated my local copy of my website
to it), where I have every page written in R Markdown, with a template.html
file, which I use as the template for pandoc to compile everything into html.
Additionally, the template.html file loads my main.css stylesheet, which means
that I can have a separate stylesheet, and have it loaded for every page;
additionally, every page has the same header and footer; as well, I don't need
much fanciness within the content of each blog post, so Markdown is a perfectly
viable language to write them in.

And this has the added benefit of, when I decide to update the stylesheets,
header, or footer, I can change one file and it will compile into every page.

This was the latest advancement I found out about in terms of Markdown/pandoc
processing, a rabbit hole which I started down in response to a few videos by
[Luke Smith](https://lukesmith.xyz), about writing presentations and pdfs in
Markdown.

During this process, I have learned a lot about the extensibility of so called
pandoc-flavored Markdown; I discovered that, just as when I used Hugo to
generate my website (also using Markdown), the section at the top (between the
two lines of three hyphens) is used for formatting purposes; for example, this
page has three variables, "title", which is the webpage title, "author", which
is me (and I am not using yet), and "date", which I have not set for any page
yet.

This actually comes shortly after me learning about
[emmet-vim](https://github.com/mattn/emmet-vim/), which actually simplified
writing plain HTML for me each time I wrote each post in HTML, as it makes
writing tags easier.

I would be curious if there's a similar plugin to make Markdown even faster
than it already is, although I'm sure I could write some macros for stuff like
hyperlinks. That said, Markdown is pretty popular, so there proably is. 

