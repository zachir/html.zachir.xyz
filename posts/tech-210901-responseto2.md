---
title: ZachIR's Blog - Response to The Linux Gardiner
author: ZachIR
draft: true
date: August 30, 2021
output:
  html_document:
    template: template.html
---
### Response to The Linux Gardiner: Re: How GNOME thinks.

Well, here we are again. It's always such a pleasure. Remember when you tried
to kill me twice?

Portal 2 reference aside, Gardiner Bryant is one of the people I have followed
since the early days in my Linux journey, back when I used and loved Unity (and
used and didn't love GNOME 3). We disagree on a lot of things, but I've never
really felt the need to make some sort of official response until today, to his
video ["How GNOME thinks."](https://www.youtube.com/watch?v=Q23rlVUyOyg).

I'm going to make a couple of assumptions before I get into this response, just
to make sure we understand the bare minimum before continuing.

First, I'm going to assume you (the reader) have experience with GNOME 3, or at
least know what it is. If you have no experience with it, that's great, because
it sucks. The fact that people think GNOME 3 is the best Linux desktop
environment is more of a statement about the quality of Linux desktop
environments than it is of any particular benefits GNOME 3 has.

Second, this is not me saying "GNOME can only think the way I think is
correct"; obviously they can do whatever they want with their project. We
*CLEARLY* have some differences of opinion, and that's fine. Libre software is
more than capable of having two conflicting viewpoints.

And now, onto the points:

- \[0:37-0:46\] "They have narrowed the scope of GNOME applications, and by
  doing this they have re-thought what desktop applications mean."

I don't know why Nautilus drew the files on the desktop; that seems like a
window manager kind of job. But this is not the only feature that has been
stripped from Nautilus, at least from what I've heard from people with
experience with GNOME. I originally had a scathing comeback here, but after
messing around with Nautilus in Solus Budgie (4.3), it does do enough for what
I would need, so I guess it's alright. I understand that a lot of GNOME users
have been frustrated by it though.

\[1:13\] Yes, in the world of libre software, a minor inconvenience is enough
to drop a feature from a project. I understand this; I, due to a personal
decision, don't support fish for any of my shell scripts. I get it. I just
think it's hilarious from an outsider's perspective, and I would imagine (and
in fact know) how annoying it would be for an actual user.

- \[1:26-1:43\] "The "traditional desktop" is dead, and it's not coming back
  (*Note: I'm talking about Windows 95 era UI patterns here, not desktop vs
  mobile*). Instead of trying to bring back old concepts like menu bars or
  status icons, invent something better from first principles." 

Mission failed successfully.

I don't understand this movement recently, where a lot of the creatives seem to
have collectively decided, "We don't want this functional stuff anymore, we
want something *new*," and in so doing they end up content with something
technically far inferior.

GNOME 3 (and 40) is:
- *MUCH* more resource intensive than GNOME 2
- closer to a familiar interface than (stock) GNOME 2 (namely, mac OS X)
- worse at theming than GNOME 2
- *MUCH* slower than GNOME 2
- less useful in pretty much every way than GNOME 2 (specifically in regards to
  the apps that have carried on from GNOME 2, not counting the new apps that
  have been made since the switch)

And what benefits are there? Well, there are now GNOME shell extensions, so if
you don't mind the javascript (which is also fully baked into GTK 3, btw) and
your extensions being broken and having to roll back GNOME 3 frequently, you
can apply some small fixes (which is the approach System76 took with Pop!\_OS).

- \[1:47-2:03\] "While other apps and desktop environments try to integrate
  every single UI paradigm that has ever been used historically throughout the
  entirety of computing history, GNOME is willing to critically examine the old
  and outdated UI paradigms, and abandon them if they need to."

So, the first part is an obvious exaggeration, but I'll take the bait and
respond to it anyways.

No, not even close. Do you know how many apps still support Qt4? Do you know
how many apps support the Trinity Desktop Environment (one of my favorite
DE's), based on Qt3, while it's still being actively developed? I'll tell you:
outside of the applications which are part of it, none. You know, people don't
still make GTK 2 apps either. Nor are most app developers trying to get their
apps to work well with DOS, or AmigaOS, or any of the early versions of Windows
(except ReactOS, which is pretty cool), or any version of Classic Mac OS.

And if you're referring to "paradigm" as the specific combination of keys and
screen layout, then yes, GNOME 3 is *different*; different doesn't always mean
*better*. And when different means that nobody wants to use it (as was the
early days of GNOME 3), I wouldn't call that a rousing success.

Now to the second part: GNOME should critically examine the intuitiveness of
their own paradigm. Sure, if you were teaching someone who had no experience
with a computer to use it, they might catch on quickly (especially if they were
still a smartphone user). *However*, I would think the target audience for a
*desktop interface* would be *desktop* users, i.e. people who already know how
to use their interface. And it's not like GNOME 3 is easy to learn; it took me
longer to learn GNOME 3 than it did dwm (yes, you can quote me on that; it's
the truth).

I don't know if you're thinking this, but *my* immediate response if I was
reading that is "but sometimes having a different UI is better!" And I agree.
As I said earlier, I really liked the Unity desktop, and this was immediately
after I switched from Windows 10. I loved how it was something totally
different, and it made it easier for me to remember "this is Linux, I do Linux
things here", vs "this is Windows, I do Windows things here." I actually
recommend people switching to Linux (depending, of course, on the person) to
pick a UI that will constantly remind them that it is not the OS they came
from, so they can learn the Linux workflows. (I recommend Budgie or Plasma,
personally).

Well, if Budgie gets off the hook, then what's wrong with GNOME? I'll tell you.

Budgie can look different from the Windows UI, but it has a similar workflow. You
don't have nearly as much of a learning curve to use it. You press the Super
(aka Windows) key to open up the menu, you click your app (or search with the
searchbar), it opens up. You drag it to the edge of the screen if you want
pseudo-tiling, and it doesn't pull up some weird thing because your cursor
touched the wrong corner. You can minimize all the apps quickly if you need to
see the desktop. Budgie even lets you move things around to get it just like
the Windows setup if you wish.

In GNOME 3, all of these are different. Pressing the Super key (at least used
to, I've heard it might be different now) brings up the app drawer, which is
almost entirely like the similar function in OS X, except it scrolls vertically
instead of horizontally. Dragging a window to the edge of the screen (again,
this might have changed in recent releases) might tile the window, it might do
this weird thing where it shows all of your windows at once, it might change
workspaces. According to the GNOME philosophy, you're not supposed to minimize
apps, just use a different workspace (although I am a fan of workspaces and not
minimizing windows for tiling wms). Also, GNOME doesn't include any way out of
the box to change its appearance. You can install the Tweak tool, but it's
unsupported and will probably break stuff. You can install extensions, but the
GNOME team is constantly breaking whatever API there is so that whenever GNOME
upgrades the extensions will probably stop working again.

- \[ 2:05-2:12\] "System-wide theming is a broken idea. If you don't like the
  way apps look, contribute to them directly (or to the platform style."

This is the most unabashedly, incompitently stupifying take I have heard in a
hot minute.

"My app doesn't work with your theme, so you shouldn't theme it."

Excuse me? It's *my* desktop. If *your* app breaks with *my* theme, then that
is *not my problem*. "That sounds completely unreasonable!" I can
already hear you responding. "How on Earth is a programmer supposed to work
around *every single theme out there*?"

Easy: they don't. In a good toolkit, there will be a theming API, and the
programmer will be able to test some basic settings to see if things will
break. However, GTK 3 (and by the sounds of it GTK 4) is *not* a good toolkit,
and because of this, every single theme is basically a hack to get apps to look
*somewhat* better than (ugh) Adwaita. Additionally, icon themes are (again) a
hack to get around the awful default icon set.

Does this problem plague Qt applications? How often do you encounter issues in
Plasma where Qt apps are unusable because of your Qt theme? I can't speak for
everyone, but I have *never* had this happen.

Additionally, do you know how hard it is to get a reasonable feature from a
GNOME app? [The Termite dev could tell you.](https://github.com/thestinger/termite).
And that's just a single library; imagine trying to convince every single app
dev to support a colorscheme. And the issue with that would be it only provides
you with the one colorscheme; imagine trying to support all of the ones people
(and distros) want to use. If only there was a way to get all of the apps to
use whatever theme the end user wanted to use. *Maybe someone should make an*
*API for that.*

- \[ 2:17-2:53 \] "I just remember back to the GNOME 2 days, when you would
  apply a dark theme to GNOME 2, and you'd open up Firefox or even Chrome, and
  input boxes in the page would be styled with the dark system theme, and you
  would end up having the website developer assuming the input boxes were going
  to be light colored, and they would style the text, but not the background
  color of the input box, so you would end up with dark text on a dark
  background, and it was... inexcusably bad."

Well, GTK2 was not fully thought through, and the theming set the default
backgrounds for web page items. 1) that is still bad design on the part of the
web devs, as well as the GNOME team, and 2) just because they did something not
quite right before, doesn't mean they should totally abandon it now. Especially
with something as fundamental and ubiquitous as theming. Again, I point to Qt:
how often do you load up a Qt browser (I can tell you from using Qutebrowser,
with the QtWebengine backend) and find stuff to be unreasonably broken? I'll
tell you: you *don't*. Now, I'm not making the claim (right this very second)
that GTK 3 is a bad toolkit, but Qt proves that you can have reliable
system-wide theming. So to completely dismiss it because *you* didn't do it
right before is asinine.

- \[ 2:57-3:09 \] "And it's not just web browsers, a lot of applications make
  assumptions about the platform style, and they will specify a text color but
  not a background color, which can end up being illegible for end users."

I talked about this before, but this is the fault of 1) developers not taking
into account things they should and hardcoding some values and not others, and
2) the toolkit for not even allowing them to if they wanted to.

- \[ 3:22-3:26 \] "Tobias also said 'Flatpak is the future of app distribution.'"

I don't even want to talk about Flatpaks right now. They're *fine* I suppose,
and I'll leave it at that for now because if I went over everything I have to
say about Flatpaks now, this page would be around 3 times as long. I will say
that it's better than Snap.

- \[ 4:12-4:21 \] "Every preference has a cost, and this cost rises
  exponentially as you add more of them. This is why we avoid preferences as
  much as possible, and focus on fixing the underlying problems instead."

I *guess* you could say that. However, removing settings that let users *fix*
the "underlying problems" that you created doesn't seem like a very nice thing
to do. Especially when you already had the options ready and being used, and
then you just take them away later.

- \[ 4:27 \] "Preferences complicate theming"

Um... no? Like, I guess, but not really. Also, man someone should really tell
Plasma they're doing it all wrong. Giving the users the choice to make the
desktop workable and maybe even look good? Nah, you need to tell the users what
they want, a la Apple.

- \[ 4:28 \] "Preferences complicate testing'

Yes, absolutely. *This* one is right on the money, and is the biggest actual
reason to limit preferences. *However*, it is not an excuse to eschew them
entirely.

- \[ 4:30 \] "They muddy up styling"

Again, I guess, but you haven't begun to prove that.

- \[ 4:30-4:34 \] "And they often act as a band-aid for fundamental issues with
  the design from the outset"

Again, you are technically correct, but they are also often a way for the user
to tailor their computer to their workflow, rather than having to tailor their
workflow to the whims of the GNOME Foundation.

- \[ 4:47-4:56 \] "There are plenty of other desktop environments for you to
  choose. If you want the exact antithesis of GNOME, something like KDE might
  be more your speed."

As a matter of fact, KDE Plasma *is* more my speed. And I agree, the beauty of
Linux is that we can choose. I am not telling anyone not to use GNOME. This
isn't Windows, where we only get one UI and no chance to improve it. If you
like GNOME, you can use it. If you like MATE, XFCE, LXQt, Plasma, Trinity,
Budgie, Cinnamon, *you* decide what you use (as long as your distro maintainers
package it).

- \[ 5:02-5:07 \] "I simply think that GNOME and KDE are about as opposite as
  you can get."

I almost agree. In terms of ethos, yeah, they are basically opposites. If you
want something truly unlike GNOME or KDE, try xmonad. (Actually, don't do that
if you're not already familiar with tiling wm's, you're probably going to have
a bad time).
