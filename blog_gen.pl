#!/usr/bin/perl
####################### intro {{{ blog_gen.pl
# This script autogenerates a blog page (and the makefile for the blog posts)
#
# Blogs should be markdown files in the posts/ directory with the
# .md extension
#
# Blogs should have at least two fields in the header:
# - title:
#   sets the title for the blog post page
# - draft
#   determines whether the page will be compiled into the website
#
# This script is libre software, and distributed in the hopes that
# it will be useful, but no guarantees are made in regards to this
# }}}

use strict;
use warnings;
use diagnostics;

use feature 'say';

# begins_with function {{{
sub begins_with {
  return substr($_[0], 0, length($_[1])) eq $_[1];
}
# }}}

# format_filename function {{{
sub format_filename {
  my $file_name = $_[0];
  my $title = "dummy title";
  my $header_line_count = 0;
  my $html_name = $file_name;
  $html_name =~ s/.md/.html/;

  open my $fh, '<', $file_name
    or die "Can't open file : $!";

  while (my $info = <$fh>) {
    chomp($info);

    if(begins_with($info, "title:")) {
      my ($dummy, $pagetitle) = split /: /, $info;
      $title = $pagetitle;
      last;
    }
    if(begins_with($info, "---")) {
      $header_line_count = $header_line_count + 1;
      if($header_line_count >= 2) {
        last;
      }
    }
  }

  close $fh or die "Couldn't close file: $!";

  return "- [" . $title . "](/" . $html_name . ")\n";
}
# }}}

# format_array function {{{
sub format_array {
  my @strings;
  foreach my $val (@_) {
    chomp $val;
    $val = "posts/" . $val;
    push @strings, format_filename($val);
  }
  return @strings;
}
# }}}

# print_array function {{{
sub print_array {
  my $text = "";
  foreach my $line (@_) {
    $text = $text . $line;
  }
  if (! $text) {
    $text = "- no posts here yet\n"
  }
  return $text;
}
# }}}

# is_draft function {{{
sub is_draft {
  my $filename = $_[0];
  my $isdraft;
  my $header_line_count = 0;
  open my $fh, '<', $filename
    or die "Can't open file $!";
  while (my $info = <$fh>){
    chomp($info);
    if(begins_with($info, "draft:")) {
      my ($dummy, $id) = split /: /, $info;
      $isdraft = $id;
      last;
    } elsif (begins_with($info, "---")) {
      $header_line_count = $header_line_count + 1;
      if ($header_line_count >= 2) {
        last;
      }
    }
  }
  close $fh or die "Can't close file $!";
  $isdraft = lc $isdraft;
  if ($isdraft eq 'false') {
    return 0;
  } else {
    return 1;
  }
}
# }}}

# generate_filelist function {{{
sub generate_filelist {
  my $mdfiles = "";
  foreach my $file (@_) {
    my $temp = $file;
    chomp($temp);
    $temp =~ s/^posts\///;
    $mdfiles = $mdfiles . " " . $temp;

  }
  return $mdfiles;
}
# }}}

# variable definitions {{{
my @files;
my @rfiles;
my @tfiles;
my @wfiles;
my @mfiles;
my @rfmt;
my @tfmt;
my @wfmt;
my @mfmt;
my $file;
my $generated_text;
my $filelist;
my $html_filelist;
my $filename;
my $index = 0;
my $blog_in_file = "blog.md";
my $blog_out_file = "blog.2.md";
my $posts_dir = "posts";
my $makefile = $posts_dir . "/Makefile";
my $template_html = "../templates/template.html";
my $religion_heading = <<"END";
#### Religion

I'm not sure what you should expect to see here, but it will be Christian
in nature. Perhaps devotionals I come across, passages I feel I should
note, something of that kind.

END
my $technology_heading = <<"END";
#### Technolody

This will likely be my reviews and guides/tutorials. Additionally, I
will make posts about projects I am developing as they come out, and new
versions, and such, as well as opinion pieces.

END
my $writing_heading = <<"END";
#### Writing

This will be updates to stories I'm writing, perhaps I will even put in
some short stories in the canon of other projects I am writing. Or maybe
I will just free write, and post it anyways.

END
my $music_heading = <<"END";
#### Music

This will be updates to my music, as well as (probably) reviews, probably of
both equipment I have and software I can get to run (probably just plugins).
Additionally, I will probably post DI tracks here during reviews, so you can
get the best picture of what the product is doing.

END
# }}}

# sort files {{{
opendir(DIR, "./posts");
@files = grep(/\.md$/,readdir(DIR));
closedir(DIR);

@files = sort @files;

foreach $file (@files) {
  $filename = "posts/" . $file;
  if (is_draft($filename)) {
    delete $files[$index];
    $index = $index + 1;
    next;
  }
  if (begins_with($file, "reli-")) {
    push @rfiles, $file . "\n";
  } elsif (begins_with($file, "tech-")) {
    push @tfiles, $file . "\n";
  } elsif (begins_with($file, "writ-")) {
    push @wfiles, $file . "\n";
  } elsif (begins_with($file, "musi-")) {
    push @mfiles, $file . "\n";
  }
  $index = $index + 1;
}

# }}}

# format arrays {{{
@rfmt = format_array(@rfiles);
@tfmt = format_array(@tfiles);
@wfmt = format_array(@wfiles);
@mfmt = format_array(@mfiles);
# }}}

# generate blog.2.md file {{{

$generated_text = $religion_heading . print_array(@rfmt) . "\n"
  . $technology_heading . print_array(@tfmt) . "\n"
  . $writing_heading . print_array(@wfmt) . "\n"
  . $music_heading . print_array(@mfmt) . "\n";

open my $fh1, '<', $blog_in_file
  or die "Can't open blog.md $!";

open my $fh2, '>', $blog_out_file
  or die "Can't open blog.2.md $!";

while(my $info = <$fh1>) {
  print $fh2 $info;
}

close $fh1 or die "Can't close blog.md $!";

print $fh2 $generated_text;

close $fh2 or die "Can't close blog.2.md $!";
# }}}

# generate posts/Makefile {{{
$filelist = generate_filelist(@rfiles) . " " . 
  generate_filelist(@tfiles) . " " .
  generate_filelist(@wfiles) . " " .
  generate_filelist(@mfiles) . "\n";

$html_filelist = $filelist;
$html_filelist =~ s/\.md/.html/g;
open my $fh3, '>', $makefile
  or die "Can't open posts/Makefile $!";

print $fh3 "DIR := ../build/" . $posts_dir . "\n";
print $fh3 "MARKDOWN_FILES := " . $filelist;
print $fh3 "HTML_FILES := " . $html_filelist;
print $fh3 "\n";
print $fh3 'all: ${DIR} ${HTML_FILES}' . "\n";
print $fh3 "\n";
foreach my $filemd (@files) {
  my $filehtml = $filemd;
  $filehtml =~ s/\.md$/.html/;
  print $fh3 $filehtml . ": " . $filemd . " " . $template_html . "\n";
  print $fh3 "\tpandoc " . $filemd . " --template=" . $template_html . " -o ../build/" . $posts_dir ."/" . $filehtml . "\n";
  print $fh3 "\n";
}

close $fh3 or die "Cannot close posts/Makefile $!";
# }}}
