---
title: ZachIR Homepage
author: ZachIR
date:
output:
  html_document:
    template: template.html
---
I am Zach (ZachIR), an adopted son of God, and a software hacker (with "hacker"
being in the sense of "someone who messes with/hacks on software", rather than
"one who breaks into things"). I use (almost) entirely Free/libre software, and
I *hate* Javascript and pretty much every proprietary operating system (except
maybe DOS).

I am (or rather, will eventually be) an author: I am writing a fiction story
called *The Festival of Red*. Unfortunately, there is no end in sight in terms
of the writing process. If anyone has kept track of my website history, they
could tell you that "pulling a Zach" basically means picking something up,
working hard on it for a little bit, then abandoning it for a while, only to
come back months later. That said, I am regularly getting *slightly* closer, so
hopefully by the time I turn 30 I will be able to have it published.  That is
the goal, anyways.

I am also a songwriter; however, I do not have a release date for the album we
are working on. I will give some details, though: we are Echoes of Sirens, an
alternative rock band. In terms of describing the music, I would have to say
that we are like Weezer, but with less people, and with darker/more emo themes.

I am also something of a software developer; I have a collection of
[scripts](https://gitlab.com/zachir/scripts), as well as a special script,
[volsv](https://gitlab.com/zachir/volsv), which can be used to control the
volume (it does rely on pamixer, amixer, and/or sndioctl, depending on the
backend).
