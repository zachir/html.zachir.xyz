---
title: About ZachIR
author: ZachIR
date: 
output:
  html_document:
    template: template.html
---

I do not talk politics on the internet.

I subscribe to the Lunduke code of conduct ("Be Excellent To Each Other").

I do not have social media (with the exception of an account I don't use on the
Fediverse, and another account on lunduke.locals.com, which 1) I would argue
doesn't count, and 2) I also seldom use).

I am a Christian, but more importantly, a follower of Christ. This does not
mean I think any better of myself than anyone else; I will be the first to
admit that I'm really not that great, but I strive to improve every day.

I publish the content of this website for my own sake. I would be happy to help
others if they find answers to something on my site, but I do not write it
specifically for others.

Now that I've gotten all of that out of the way, welcome to my humble little
corner of the internet!

### Contact Me

I am available on my self-hosted XMPP server, with the jid
zachir@chat.zachir.xyz (no, it is not open to registration).

### Recommendations

You should not put *too* much stake into my software recommendations, as I
don't do a whole lot of research into things like sustainability or developers,
but I do recommend what I actually use, as it *mostly* works well.

#### Operating System

This is a tricky one, and I have multiple answers. First of all:

For the normie/tech-illiterate, I recommend [Linux
Mint](https://linuxmint.com), specifically, the latest LTS release at whatever
time I'm making the recommendation is. It comes with just about everything the
"average" person would need (i.e. a normie web browser, Firefox). Additionally,
it's pretty easy to get Steam, WINE, Lutris, etc. on it, which means it
*could* also be used for gaming; however, for gaming, I would recommend
the next distro.

For the gamer, I recommend [Pop!\_OS](https://pop.system76.com). It comes with
two different releases for NVidia users, and AMD/Intel users. Alternatively, if
said gamer is feeling a little more adventurous, I would additionally recommend
Manjaro. There are some specific gaming-related Linux distros, but I don't have
experience with any of them, personally.

For the more advanced user, I have two recommendations: if at all possible, I
strongly recommend [OpenBSD](https://www.openbsd.org), as it has been a delight
every time I've used it; unfortunately, it does lack certain things, and
doesn't have nearly as much hardware support as Linux. And for those whom that
doesn't work for, I recommend Artix Linux (which is the distrobution I am
currently running). It's basically Arch, minus systemd (the version I use comes
with Runit).

#### Web Browser

I don't have too much expertise in this area, so I default to the opinions of
others here. But in terms of what I personally use, I use
[Qutebrowser](https://qutebrowser.org) for most things, and
[Librewolf](https://librewolf-community.gitlab.io) for more security (because I
have a bunch of extensions). I dislike the direction Firefox is going, and I
don't want to support the Blink engine (unfortunately, the Webengine backend of
Qutebrowser is based off of it), and I like Palemoon/Basilisk, but they don't
have all of the extensions available I would like (especially Basilisk).

#### Chat Clients

For XMPP, I recommend [Profanity](https://www,profanity.im) as a desktop
client, and [blabber.im](https://blabber.im) as an Android client.
[Conversations](https://conversations.im) is fine, but I noticed some
unsolicited internet connections behind my phone's firewall; that said, it is
on [F-Droid](https://www.f-droid.org/), so there *shouldn't* be any
trackers or naughtiness.

For IRC (which I do use), I recommend [Irssi](https://irssi.org) as a desktop
client. I understand [Weechat](https://weechat.org/) is pretty cool, and it has
a Matrix plugin, but I still like the simplicity of Irssi.

For Matrix, I recommend [gomuks](https://github.com/tulir/gomuks), due to there
not really being a lot of options for TUI Matrix, but if you want the full
feature set, there isn't really anything other than
[Element](https://element.io), which is both a mobile and desktop client.

#### Others

I recommend either dwm and xmonad for tiling window managers.  There's not much
to say about them except that they work, and they're really stable. Also, both
are highly configurable (although you need to be able to program to configure
either, c for dwm and haskell for xmonad).

I recommend using Invidious rather than YouTube, the specific instance I use is
[YewTu.be](https://yewtu.be).

### Donations

I don't really have any services I offer yet, but I am working on plans to
start making videos on some media platform. I also provide my
[dotfiles](https://gitlab.com/zachir/dotfiles), as well as my
[scripts](https://gitlab.com/zachir/scripts), and a special script I wrote
called [volsv](https://gitlab.com/zachir/volsv), which can be used to set or
check the volume, and toggle mute (and mic mute).

I am still working on writing a fiction story, called *The Festival of
Red*, which I would like to finish sometime before I turn 30. I cannot
promise any donations will go into helping me write it, but I also cannot
promise that they *won't* go into it, either.

The donation links for BTC and XMR are at the bottom of every page, if you want
to donate.
